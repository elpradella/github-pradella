const colors = require('./assets/sassVariables');
const vars = require('postcss-simple-vars');

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'GitHub-Pradella',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A repo for all things GitHub' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: colors.emerald },
  /*
  ** Build configuration
  */
  build: {
    postcss: [
      vars({
        variables: function () {
            return colors;
        }
      }),
      require('postcss-import')(),
      require('autoprefixer')(),
      require('postcss-nested')(),
    ]
  }
}