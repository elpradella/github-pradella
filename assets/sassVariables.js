module.exports = {
    //Spacing
    spaceSS: "5px",
    spaceS: "10px",
    space: "15px",
    spaceX: "30px",
    spaceXX: "45px",
    //Colors
    alphaLink: "rgba(255,255,255, .08)",
    grayDark: "#2A2A2A",
    grayDarker: "#1A1A1A",
    black: "#000000",
    yellow: "#E3AD09",
    blue: "#90CAF0",
    blueDark: "#264F78",
    emerald: "#00FFCB"
}